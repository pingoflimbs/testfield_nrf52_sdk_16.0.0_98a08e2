/**
 * Copyright (c) 2014 - 2019, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/** @file
 *
 * @defgroup ble_sdk_app_hids_mouse_main main.c
 * @{
 * @ingroup ble_sdk_app_hids_mouse
 * @brief HID Mouse Sample Application main file.
 *
 * This file contains is the source code for a sample application using the HID, Battery and Device
 * Information Service for implementing a simple mouse functionality. This application uses the
 * @ref app_scheduler.
 *
 * Also it would accept pairing requests from any peer device. This implementation of the
 * application will not know whether a connected central is a known device or not.
 */

#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "nrf_sdm.h"
#include "app_error.h"
#include "ble.h"
#include "ble_err.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_hids.h"
#include "ble_bas.h"
#include "ble_dis.h"
#include "ble_conn_params.h"
#include "sensorsim.h"
#include "bsp_btn_ble.h"
#include "app_scheduler.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "app_timer.h"
#include "peer_manager.h"
#include "ble_advertising.h"
#include "fds.h"
#include "ble_conn_state.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_qwr.h"
#include "nrf_pwr_mgmt.h"
#include "peer_manager_handler.h"

#include "nrf_gpio.h"
#include "nrf_drv_twi.h"
#include "nrf_delay.h"
#include  "nrf_gpiote.h"

#include "nrf_drv_timer.h"


#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"



/* LOG defines */
#define __FILENAME__ (strchr(__FILE__, '\\') ? strrchr(__FILE__,'\\')+1 : strrchr(__FILE__,'/')+1) // string.h
#define FLIMBS_LOG(fmt,...)\
do{\
    SEGGER_RTT_SetTerminal(0);\
    char dbg_str[256];\
    sprintf(dbg_str, "[%4d:%14.14s:%-18.18s:%04d] " fmt "\n", 0, __FILENAME__, __func__, __LINE__, ##__VA_ARGS__);\
    SEGGER_RTT_printf(0,"E:%s",dbg_str);\
} while(0)

#define LOG_temp(fmt,...) //FLIMBS_LOG(fmt,##__VA_ARGS__)
#define LOG_pos(fmt,...) FLIMBS_LOG(fmt,##__VA_ARGS__)

#define DEVICE_NAME                     "MKB420"                                /**< Name of device. Will be included in the advertising data. */
#define MANUFACTURER_NAME               "INNOPRESSO"                       /**< Manufacturer. Will be passed to Device Information Service. */

#define APP_BLE_OBSERVER_PRIO           3                                           /**< Application's BLE observer priority. You shouldn't need to modify this value. */
#define APP_BLE_CONN_CFG_TAG            1                                           /**< A tag identifying the SoftDevice BLE configuration. */

#define BATTERY_LEVEL_MEAS_INTERVAL     APP_TIMER_TICKS(2000)                       /**< Battery level measurement interval (ticks). */
#define MIN_BATTERY_LEVEL               81                                          /**< Minimum simulated battery level. */
#define MAX_BATTERY_LEVEL               100                                         /**< Maximum simulated battery level. */
#define BATTERY_LEVEL_INCREMENT         1                                           /**< Increment between each simulated battery level measurement. */

#define PNP_ID_VENDOR_ID_SOURCE         0x02                                        /**< Vendor ID Source. */
#define PNP_ID_VENDOR_ID                0x1915                                      /**< Vendor ID. */
#define PNP_ID_PRODUCT_ID               0xEEEE                                      /**< Product ID. */
#define PNP_ID_PRODUCT_VERSION          0x0001                                      /**< Product Version. */

/*lint -emacro(524, MIN_CONN_INTERVAL) // Loss of precision */
#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(7.5, UNIT_1_25_MS)            /**< Minimum connection interval (7.5 ms). */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(15, UNIT_1_25_MS)             /**< Maximum connection interval (15 ms). */
#define SLAVE_LATENCY                   20                                          /**< Slave latency. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(3000, UNIT_10_MS)             /**< Connection supervisory timeout (3000 ms). */

#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000)                       /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000)                      /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAM_UPDATE_COUNT     3                                           /**< Number of attempts before giving up the connection parameter negotiation. */

#define SEC_PARAM_BOND                  1                                           /**< Perform bonding. */
#define SEC_PARAM_MITM                  0                                           /**< Man In The Middle protection not required. */
#define SEC_PARAM_LESC                  0                                           /**< LE Secure Connections not enabled. */
#define SEC_PARAM_KEYPRESS              0                                           /**< Keypress notifications not enabled. */
#define SEC_PARAM_IO_CAPABILITIES       BLE_GAP_IO_CAPS_NONE                        /**< No I/O capabilities. */
#define SEC_PARAM_OOB                   0                                           /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE          7                                           /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE          16                                          /**< Maximum encryption key size. */

#define SWIFT_PAIR_SUPPORTED            1                                           /**< Swift Pair feature is supported. */
#if SWIFT_PAIR_SUPPORTED == 1
#define MICROSOFT_VENDOR_ID             0x0006                                      /**< Microsoft Vendor ID.*/
#define MICROSOFT_BEACON_ID             0x03                                        /**< Microsoft Beacon ID, used to indicate that Swift Pair feature is supported. */
#define MICROSOFT_BEACON_SUB_SCENARIO   0x00                                        /**< Microsoft Beacon Sub Scenario, used to indicate how the peripheral will pair using Swift Pair feature. */
#define RESERVED_RSSI_BYTE              0x80                                        /**< Reserved RSSI byte, used to maintain forwards and backwards compatibility. */
#endif

#define MOVEMENT_SPEED                  5                                           /**< Number of pixels by which the cursor is moved each time a button is pushed. */
#define INPUT_REPORT_COUNT              3                                           /**< Number of input reports in this application. */
#define INPUT_REP_BUTTONS_LEN           3                                           /**< Length of Mouse Input Report containing button data. */
#define INPUT_REP_MOVEMENT_LEN          3                                           /**< Length of Mouse Input Report containing movement data. */
#define INPUT_REP_MEDIA_PLAYER_LEN      1                                           /**< Length of Mouse Input Report containing media player data. */
#define INPUT_REP_BUTTONS_INDEX         0                                           /**< Index of Mouse Input Report containing button data. */
#define INPUT_REP_MOVEMENT_INDEX        1                                           /**< Index of Mouse Input Report containing movement data. */
#define INPUT_REP_MPLAYER_INDEX         2                                           /**< Index of Mouse Input Report containing media player data. */
#define INPUT_REP_REF_BUTTONS_ID        1                                           /**< Id of reference to Mouse Input Report containing button data. */
#define INPUT_REP_REF_MOVEMENT_ID       2                                           /**< Id of reference to Mouse Input Report containing movement data. */
#define INPUT_REP_REF_MPLAYER_ID        3                                           /**< Id of reference to Mouse Input Report containing media player data. */

#define BASE_USB_HID_SPEC_VERSION       0x0101                                      /**< Version number of base USB HID Specification implemented by this application. */

#define SCHED_MAX_EVENT_DATA_SIZE       APP_TIMER_SCHED_EVENT_DATA_SIZE             /**< Maximum size of scheduler events. */
#ifdef SVCALL_AS_NORMAL_FUNCTION
#define SCHED_QUEUE_SIZE                20                                          /**< Maximum number of events in the scheduler queue. More is needed in case of Serialization. */
#else
#define SCHED_QUEUE_SIZE                10                                          /**< Maximum number of events in the scheduler queue. */
#endif

#define DEAD_BEEF                       0xDEADBEEF                                  /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

#define APP_ADV_FAST_INTERVAL           0x0028                                      /**< Fast advertising interval (in units of 0.625 ms. This value corresponds to 25 ms.). */
#define APP_ADV_SLOW_INTERVAL           0x00A0                                      /**< Slow advertising interval (in units of 0.625 ms. This value corresponds to 100 ms.). */

#define APP_ADV_FAST_DURATION           3000                                        /**< The advertising duration of fast advertising in units of 10 milliseconds. */
#define APP_ADV_SLOW_DURATION           18000                                       /**< The advertising duration of slow advertising in units of 10 milliseconds. */

/* TWI instance ID. */
#if TWI0_ENABLED
#define TWI_INSTANCE_ID     0
#elif TWI1_ENABLED
#define TWI_INSTANCE_ID     1
#endif


/* Number of possible TWI addresses. */
#define TWI_ADDRESSES      127

#define SCL 2
#define SDA 47
#define INT 45
#define RED_1 9
#define GRN_1 10
#define BLU_1 42
#define RED_2 26
#define GRN_2 4
#define BLU_2 6
#define RED_3 8
#define GRN_3 41
#define BLU_3 12
#define RED_4 34
#define GRN_4 36
#define BLU_4 38

#define BANK_USER 0x06

#define REG_GET_REPORT1 0x08
#define REG_GET_REPORT2 0x00
#define REG_READ_REPORT 0x0B

#define REG_BOOT_READY 0x70
#define REG_STATUS 0x71
#define REG_SLEEP_STATUS 0x7C


#define REG_HOST_RESET 0x7A
#define REG_HOST_RESET_ENABLE 0x7B
#define REG_DEEPSLEEP 0x7C

#define REG_CHANGEBANK 0x7F

#define REG_INDIRECT_BANK 0x73
#define REG_INDIRECT_BANK_OFFSET 0x80
#define REG_INDIRECT_REG 0x74
#define REG_INDIRECT_DATA 0x75



APP_TIMER_DEF(m_battery_timer_id);                                                  /**< Battery timer. */
APP_TIMER_DEF(m_handler_timer_id);
BLE_BAS_DEF(m_bas);                                                                 /**< Battery service instance. */
BLE_HIDS_DEF(m_hids,                                                                /**< HID service instance. */
             NRF_SDH_BLE_TOTAL_LINK_COUNT,
             INPUT_REP_BUTTONS_LEN,
             INPUT_REP_MOVEMENT_LEN,
             INPUT_REP_MEDIA_PLAYER_LEN);
NRF_BLE_GATT_DEF(m_gatt);                                                           /**< GATT module instance. */
NRF_BLE_QWR_DEF(m_qwr);                                                             /**< Context for the Queued Write module.*/
BLE_ADVERTISING_DEF(m_advertising);                                                 /**< Advertising module instance. */

static bool              m_in_boot_mode = false;                                    /**< Current protocol mode. */
static uint16_t          m_conn_handle  = BLE_CONN_HANDLE_INVALID;                  /**< Handle of the current connection. */
static pm_peer_id_t      m_peer_id;                                                 /**< Device reference handle to the current bonded central. */
static sensorsim_cfg_t   m_battery_sim_cfg;                                         /**< Battery Level sensor simulator configuration. */
static sensorsim_state_t m_battery_sim_state;                                       /**< Battery Level sensor simulator state. */
static ble_uuid_t        m_adv_uuids[] =                                            /**< Universally unique service identifiers. */
{
    {BLE_UUID_HUMAN_INTERFACE_DEVICE_SERVICE, BLE_UUID_TYPE_BLE}
};

#if SWIFT_PAIR_SUPPORTED == 1
static uint8_t m_sp_payload[] =                                                     /**< Payload of advertising data structure for Microsoft Swift Pair feature. */
{
    MICROSOFT_BEACON_ID,
    MICROSOFT_BEACON_SUB_SCENARIO,
    RESERVED_RSSI_BYTE
};
static ble_advdata_manuf_data_t m_sp_manuf_advdata =                                /**< Advertising data structure for Microsoft Swift Pair feature. */
{
    .company_identifier = MICROSOFT_VENDOR_ID,
    .data               =
    {
        .size   = sizeof(m_sp_payload),
        .p_data = &m_sp_payload[0]
    }
};
static ble_advdata_t m_sp_advdata;
#endif


typedef struct 
{
	uint8_t		touchID:4   ;    // An arbitrary ID tag associated with a finger ( 0 ~ 9)
	uint8_t		confidence:1; // no_event(0), touch_down(1), significant_displacement(2), lift_off(3)
	uint8_t		tip:1		; // touch is currently on the panel
	uint8_t		res:2		; // *
	uint16_t	x;			  // 16bit x-coordinates in pixels
	uint16_t	y;			  // 16bit y-coordinates in pixels
	uint16_t	z;			  // 16bit z-coordinates in pixels
	uint16_t	area;			  // 16bit y-coordinates in pixels
} pixart_touch_info_t;

typedef struct {
	uint8_t	gesture;
	uint8_t             button;	
    uint8_t             touch_count;
	pixart_touch_info_t	info[18];
} pixart_touch_packet_t;


/* TWI instance. */
static const nrf_drv_twi_t m_twi = NRF_DRV_TWI_INSTANCE(TWI_INSTANCE_ID);
static uint8_t prj_version_old[] = {0xFF,0xFF};

void twi_init (void)
{
    ret_code_t err_code;

    const nrf_drv_twi_config_t twi_config = {
       .scl                = 2,
       .sda                = 47,
       .frequency          = NRF_DRV_TWI_FREQ_400K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH,
       .clear_bus_init     = true
    };

    err_code = nrf_drv_twi_init(&m_twi, &twi_config, NULL, NULL);
    APP_ERROR_CHECK(err_code);

    nrf_drv_twi_enable(&m_twi);
}





void check_handler();
void check();
bool pct_read_status(); 
bool isLoop= false;


static void on_hids_evt(ble_hids_t * p_hids, ble_hids_evt_t * p_evt);


/**@brief Callback function for asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in]   line_num   Line number of the failing ASSERT call.
 * @param[in]   file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}


/**@brief Function for setting filtered whitelist.
 *
 * @param[in] skip  Filter passed to @ref pm_peer_id_list.
 */
static void whitelist_set(pm_peer_id_list_skip_t skip)
{
    pm_peer_id_t peer_ids[BLE_GAP_WHITELIST_ADDR_MAX_COUNT];
    uint32_t     peer_id_count = BLE_GAP_WHITELIST_ADDR_MAX_COUNT;

    ret_code_t err_code = pm_peer_id_list(peer_ids, &peer_id_count, PM_PEER_ID_INVALID, skip);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_INFO("\tm_whitelist_peer_cnt %d, MAX_PEERS_WLIST %d",
                   peer_id_count + 1,
                   BLE_GAP_WHITELIST_ADDR_MAX_COUNT);

    err_code = pm_whitelist_set(peer_ids, peer_id_count);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for setting filtered device identities.
 *
 * @param[in] skip  Filter passed to @ref pm_peer_id_list.
 */
static void identities_set(pm_peer_id_list_skip_t skip)
{
    pm_peer_id_t peer_ids[BLE_GAP_DEVICE_IDENTITIES_MAX_COUNT];
    uint32_t     peer_id_count = BLE_GAP_DEVICE_IDENTITIES_MAX_COUNT;

    ret_code_t err_code = pm_peer_id_list(peer_ids, &peer_id_count, PM_PEER_ID_INVALID, skip);
    APP_ERROR_CHECK(err_code);

    err_code = pm_device_identities_list_set(peer_ids, peer_id_count);
    APP_ERROR_CHECK(err_code);
}


/**@brief Clear bond information from persistent storage.
 */
static void delete_bonds(void)
{
    ret_code_t err_code;

    NRF_LOG_INFO("Erase bonds!");

    err_code = pm_peers_delete();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for starting advertising.
 */
static void advertising_start(bool erase_bonds)
{
    if (erase_bonds == true)
    {
        delete_bonds();
        // Advertising is started by PM_EVT_PEERS_DELETE_SUCCEEDED event.
    }
    else
    {
        whitelist_set(PM_PEER_ID_LIST_SKIP_NO_ID_ADDR);

        ret_code_t ret = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
        APP_ERROR_CHECK(ret);
    }
}


/**@brief Function for handling Peer Manager events.
 *
 * @param[in] p_evt  Peer Manager event.
 */
static void pm_evt_handler(pm_evt_t const * p_evt)
{
    pm_handler_on_pm_evt(p_evt);
    pm_handler_flash_clean(p_evt);

    switch (p_evt->evt_id)
    {
        case PM_EVT_PEERS_DELETE_SUCCEEDED:
            advertising_start(false);
            break;

        case PM_EVT_PEER_DATA_UPDATE_SUCCEEDED:
            if (     p_evt->params.peer_data_update_succeeded.flash_changed
                 && (p_evt->params.peer_data_update_succeeded.data_id == PM_PEER_DATA_ID_BONDING))
            {
                NRF_LOG_INFO("New Bond, add the peer to the whitelist if possible");
                // Note: You should check on what kind of white list policy your application should use.

                whitelist_set(PM_PEER_ID_LIST_SKIP_NO_ID_ADDR);
            }
            break;

        default:
            break;
    }
}


/**@brief Function for handling Service errors.
 *
 * @details A pointer to this function will be passed to each service which may need to inform the
 *          application about an error.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void service_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for handling advertising errors.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void ble_advertising_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for performing a battery measurement, and update the Battery Level characteristic in the Battery Service.
 */
static void battery_level_update(void)
{
    ret_code_t err_code;
    uint8_t  battery_level;

    battery_level = (uint8_t)sensorsim_measure(&m_battery_sim_state, &m_battery_sim_cfg);

    err_code = ble_bas_battery_level_update(&m_bas, battery_level, BLE_CONN_HANDLE_ALL);
    if ((err_code != NRF_SUCCESS) &&
        (err_code != NRF_ERROR_BUSY) &&
        (err_code != NRF_ERROR_RESOURCES) &&
        (err_code != NRF_ERROR_FORBIDDEN) &&
        (err_code != NRF_ERROR_INVALID_STATE) &&
        (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING)
       )
    {
        APP_ERROR_HANDLER(err_code);
    }
}


/**@brief Function for handling the Battery measurement timer timeout.
 *
 * @details This function will be called each time the battery level measurement timer expires.
 *
 * @param[in]   p_context   Pointer used for passing some arbitrary information (context) from the
 *                          app_start_timer() call to the timeout handler.
 */
static void battery_level_meas_timeout_handler(void * p_context)
{
    UNUSED_PARAMETER(p_context);
    battery_level_update();
}


/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module.
 */


static void timers_init(void)
{
    ret_code_t err_code;

    err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);

    // Create battery timer.
    err_code = app_timer_create(&m_battery_timer_id,
                                APP_TIMER_MODE_REPEATED,
                                battery_level_meas_timeout_handler);


    err_code = app_timer_create(&m_handler_timer_id,
                                APP_TIMER_MODE_REPEATED,
                                check);
    
    APP_ERROR_CHECK(err_code);

}



void led_init()
{

  nrf_gpio_pin_dir_set(RED_1,NRF_GPIO_PIN_DIR_OUTPUT);
  nrf_gpio_pin_dir_set(GRN_1,NRF_GPIO_PIN_DIR_OUTPUT);
  nrf_gpio_pin_dir_set(BLU_1,NRF_GPIO_PIN_DIR_OUTPUT);

  nrf_gpio_pin_dir_set(RED_2,NRF_GPIO_PIN_DIR_OUTPUT);
  nrf_gpio_pin_dir_set(GRN_2,NRF_GPIO_PIN_DIR_OUTPUT);
  nrf_gpio_pin_dir_set(BLU_2,NRF_GPIO_PIN_DIR_OUTPUT);


  nrf_gpio_pin_dir_set(RED_3,NRF_GPIO_PIN_DIR_OUTPUT);
  nrf_gpio_pin_dir_set(GRN_3,NRF_GPIO_PIN_DIR_OUTPUT);
  nrf_gpio_pin_dir_set(BLU_3,NRF_GPIO_PIN_DIR_OUTPUT);


  nrf_gpio_pin_dir_set(RED_4,NRF_GPIO_PIN_DIR_OUTPUT);
  nrf_gpio_pin_dir_set(GRN_4,NRF_GPIO_PIN_DIR_OUTPUT);
  nrf_gpio_pin_dir_set(BLU_4,NRF_GPIO_PIN_DIR_OUTPUT);

  
    nrf_gpio_pin_write(RED_1,1);
    nrf_gpio_pin_write(RED_2,1);
    nrf_gpio_pin_write(RED_3,1);
    nrf_gpio_pin_write(RED_4,1);
    nrf_gpio_pin_write(GRN_1,1);
    nrf_gpio_pin_write(GRN_2,1);
    nrf_gpio_pin_write(GRN_3,1);
    nrf_gpio_pin_write(GRN_4,1);
    nrf_gpio_pin_write(BLU_1,1);
    nrf_gpio_pin_write(BLU_2,1);
    nrf_gpio_pin_write(BLU_3,1);
    nrf_gpio_pin_write(BLU_4,0);

}


/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
static void gap_params_init(void)
{
    ret_code_t              err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    err_code = sd_ble_gap_appearance_set(BLE_APPEARANCE_HID_KEYBOARD);
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the GATT module.
 */
static void gatt_init(void)
{
    ret_code_t err_code = nrf_ble_gatt_init(&m_gatt, NULL);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling Queued Write Module errors.
 *
 * @details A pointer to this function will be passed to each service which may need to inform the
 *          application about an error.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void nrf_qwr_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing the Queued Write Module.
 */
static void qwr_init(void)
{
    ret_code_t         err_code;
    nrf_ble_qwr_init_t qwr_init_obj = {0};

    qwr_init_obj.error_handler = nrf_qwr_error_handler;

    err_code = nrf_ble_qwr_init(&m_qwr, &qwr_init_obj);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing Device Information Service.
 */
static void dis_init(void)
{
    ret_code_t       err_code;
    ble_dis_init_t   dis_init_obj;
    ble_dis_pnp_id_t pnp_id;

    pnp_id.vendor_id_source = PNP_ID_VENDOR_ID_SOURCE;
    pnp_id.vendor_id        = PNP_ID_VENDOR_ID;
    pnp_id.product_id       = PNP_ID_PRODUCT_ID;
    pnp_id.product_version  = PNP_ID_PRODUCT_VERSION;

    memset(&dis_init_obj, 0, sizeof(dis_init_obj));

    ble_srv_ascii_to_utf8(&dis_init_obj.manufact_name_str, MANUFACTURER_NAME);
    dis_init_obj.p_pnp_id = &pnp_id;

    dis_init_obj.dis_char_rd_sec = SEC_JUST_WORKS;

    err_code = ble_dis_init(&dis_init_obj);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing Battery Service.
 */
static void bas_init(void)
{
    ret_code_t     err_code;
    ble_bas_init_t bas_init_obj;

    memset(&bas_init_obj, 0, sizeof(bas_init_obj));

    bas_init_obj.evt_handler          = NULL;
    bas_init_obj.support_notification = true;
    bas_init_obj.p_report_ref         = NULL;
    bas_init_obj.initial_batt_level   = 100;

    bas_init_obj.bl_rd_sec        = SEC_JUST_WORKS;
    bas_init_obj.bl_cccd_wr_sec   = SEC_JUST_WORKS;
    bas_init_obj.bl_report_rd_sec = SEC_JUST_WORKS;

    err_code = ble_bas_init(&m_bas, &bas_init_obj);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing HID Service.
 */
static void hids_init(void)
{
    ret_code_t                err_code;
    ble_hids_init_t           hids_init_obj;
    ble_hids_inp_rep_init_t * p_input_report;
    uint8_t                   hid_info_flags;

    static ble_hids_inp_rep_init_t inp_rep_array[INPUT_REPORT_COUNT];
    static uint8_t rep_map_data[] =
{

	/*

	usage page
	usage keyboard
	*/
	/////////////////////////////////////////////////////////////////////////
	// Keyboard
	// bb bbbbb x
	// xx bbbbbb
	/////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////
	// Keyboard
	/////////////////////////////////////////////////////////////////////////
	0x05, 0x01,       // Usage Page (Generic Desktop)
	0x09, 0x06,       // Usage (Keyboard)
	0xA1, 0x01,       // Collection (Application)

	// Report ID 1: Keyboard
	0x85, 1,  	// Report Id (1)
	0x05, 0x07,       // Usage Page (Key Codes)
		0x19, 0xe0,       // Usage Minimum (224)
		0x29, 0xe7,       // Usage Maximum (231)
		0x15, 0x00,       // Logical Minimum (0)
		0x25, 0x01,       // Logical Maximum (1)
		0x75, 0x01,       // Report Size (1)
		0x95, 0x08,       // Report Count (8)
		0x81, 0x02,       // Input (Data, Variable, Absolute)
	
		0x95, 0x01,       // Report Count (1)
		0x75, 0x08,       // Report Size (8)
		0x81, 0x01,       // Input (Constant) reserved byte(1)

		0x95, 0x05,       // Report Count (5)
		0x75, 0x01,       // Report Size (1)
		0x05, 0x08,       // Usage Page (Page# for LEDs)
		0x19, 0x01,       // Usage Minimum (1)
		0x29, 0x05,       // Usage Maximum (5)
		0x91, 0x02,       // Output (Data, Variable, Absolute), Led report

		0x95, 0x01,       // Report Count (1)
		0x75, 0x03,       // Report Size (3)
		0x91, 0x01,       // Output (Data, Variable, Absolute), Led report padding

		0x05, 0x07,        //   Usage Page (Kbrd/Keypad)
		0x19, 0x00,        //   Usage Minimum (0x00)
		0x2A, 0xFF, 0x00,  //   Usage Maximum (0xFF)
		0x95, 0x05,        //   Report Count (5)
		0x75, 0x08,        //   Report Size (8)
		0x15, 0x00,        //   Logical Minimum (0)
		0x26, 0xFF, 0x00,  //   Logical Maximum (255)
		0x81, 0x00,        //   Input (Null Position)
		
		0x05, 0xFF,        //   Usage Page (Reserved 0xFF)
		0x09, 0x03,        //   Usage (0x03)
		0x75, 0x08,        //   Report Size (8)
		0x95, 0x01,        //   Report Count (1)
		0x81, 0x02,        //   Input (Position)

	0xC0,              // End Collection (Application)

	/////////////////////////////////////////////////////////////////////////
	// Mouse: Click/wheel/Move
	/////////////////////////////////////////////////////////////////////////
	0x05, 0x01, // Usage Page (Generic Desktop)
	0x09, 0x02, // Usage (Mouse)
	0xA1, 0x01, // Collection (Application)
		0x85, 2, 	// Report Id 2		// Report ID 2: Mouse buttons + scroll/pan
		0x09, 0x01,       // Usage (Pointer)
		0xA1, 0x00,       // Collection (Physical)
			0x95, 0x05,       // Report Count (3)
			0x75, 0x01,       // Report Size (1)
			0x05, 0x09,       // Usage Page (Buttons)
			0x19, 0x01,       // Usage Minimum (01)
			0x29, 0x05,       // Usage Maximum (05)
			0x15, 0x00,       // Logical Minimum (0)
			0x25, 0x01,       // Logical Maximum (1)
			0x81, 0x02,       // Input (Data, Variable, Absolute)

			0x95, 0x01,       // Report Count (1)
			0x75, 0x03,       // Report Size (3)
			0x81, 0x01,       // Input (Constant) for padding


			0x75, 0x08,       // Report Size (8)
			0x95, 0x01,       // Report Count (1)
			0x05, 0x01,       // Usage Page (Generic Desktop)
			0x09, 0x38,       // Usage (Wheel)
			0x15, 0x81,       // Logical Minimum (-127)
			0x25, 0x7F,       // Logical Maximum (127)
			0x81, 0x06,       // Input (Data, Variable, Relative)

			0x05, 0x0C,       // Usage Page (Consumer)
			0x0A, 0x38, 0x02, // Usage (Undefined)
			0x95, 0x01,       // Report Count (1)
			0x81, 0x06,       // Input (Data,Value,Relative,Bit Field), 0xFF for left, or 0x00 for no scroll, or 0x01 for right
		0xC0,             // End Collection (Physical)
			
			// Report ID 3: Mouse motion
			0x85, 3, 	// Report Id 3
			0x09, 0x01,       // Usage (Pointer)
			0xA1, 0x00,       // Collection (Physical)
				0x75, 0x0C,       // Report Size (12)
				0x95, 0x02,       // Report Count (2)
				0x05, 0x01,       // Usage Page (Generic Desktop)
				0x09, 0x30,       // Usage (X)
				0x09, 0x31,       // Usage (Y)
				0x16, 0x01, 0xF8, // Logical maximum (2047)
				0x26, 0xFF, 0x07, // Logical minimum (-2047)
				0x81, 0x06,       // Input (Data, Variable, Relative)
		0xC0,             // End Collection (Physical)
	0xC0,             // End Collection (Application)
	
	/////////////////////////////////////////////////////////////////////////
	// Consumer control: volume up/down
	/////////////////////////////////////////////////////////////////////////
    0x05, 0x0C,    		// Usage Page (Consumer Devices)
    0x09, 0x01,       // Usage (Consumer Control)
    0xA1, 0x01,       // Collection (Application)
		0x85, 4,//      Report ID 4 (CONSUMER_CTRL_IN_REP_ID)
		0x75, 12,					//      Report Size (CONSUMER_CTRL_IN_REP_SIZE)
		0x95, 2,					//      Report Count (CONSUMER_CTRL_IN_REP_COUNT)
		0x15, 0x00,       //      Logical Minimum (0)
		0x26, 0xFF, 0x07, //      Logical Maximum (2047)
		0x19, 0x00,       //      Usage Minimum (0)
		0x2A, 0xFF, 0x07, //      Usage Maximum (2047)
		0x81, 0x00,       //      Input (Data, Ary, Abs)
    0xC0,          		// End Collection
};

    memset(inp_rep_array, 0, sizeof(inp_rep_array));
    // Initialize HID Service.
    p_input_report                      = &inp_rep_array[INPUT_REP_BUTTONS_INDEX];
    p_input_report->max_len             = INPUT_REP_BUTTONS_LEN;
    p_input_report->rep_ref.report_id   = INPUT_REP_REF_BUTTONS_ID;
    p_input_report->rep_ref.report_type = BLE_HIDS_REP_TYPE_INPUT;

    p_input_report->sec.cccd_wr = SEC_JUST_WORKS;
    p_input_report->sec.wr      = SEC_JUST_WORKS;
    p_input_report->sec.rd      = SEC_JUST_WORKS;

    p_input_report                      = &inp_rep_array[INPUT_REP_MOVEMENT_INDEX];
    p_input_report->max_len             = INPUT_REP_MOVEMENT_LEN;
    p_input_report->rep_ref.report_id   = INPUT_REP_REF_MOVEMENT_ID;
    p_input_report->rep_ref.report_type = BLE_HIDS_REP_TYPE_INPUT;

    p_input_report->sec.cccd_wr = SEC_JUST_WORKS;
    p_input_report->sec.wr      = SEC_JUST_WORKS;
    p_input_report->sec.rd      = SEC_JUST_WORKS;

    p_input_report                      = &inp_rep_array[INPUT_REP_MPLAYER_INDEX];
    p_input_report->max_len             = INPUT_REP_MEDIA_PLAYER_LEN;
    p_input_report->rep_ref.report_id   = INPUT_REP_REF_MPLAYER_ID;
    p_input_report->rep_ref.report_type = BLE_HIDS_REP_TYPE_INPUT;

    p_input_report->sec.cccd_wr = SEC_JUST_WORKS;
    p_input_report->sec.wr      = SEC_JUST_WORKS;
    p_input_report->sec.rd      = SEC_JUST_WORKS;

    hid_info_flags = HID_INFO_FLAG_REMOTE_WAKE_MSK | HID_INFO_FLAG_NORMALLY_CONNECTABLE_MSK;

    memset(&hids_init_obj, 0, sizeof(hids_init_obj));

    hids_init_obj.evt_handler                    = on_hids_evt;
    hids_init_obj.error_handler                  = service_error_handler;
    hids_init_obj.is_kb                          = false;
    hids_init_obj.is_mouse                       = true;
    hids_init_obj.inp_rep_count                  = INPUT_REPORT_COUNT;
    hids_init_obj.p_inp_rep_array                = inp_rep_array;
    hids_init_obj.outp_rep_count                 = 0;
    hids_init_obj.p_outp_rep_array               = NULL;
    hids_init_obj.feature_rep_count              = 0;
    hids_init_obj.p_feature_rep_array            = NULL;
    hids_init_obj.rep_map.data_len               = sizeof(rep_map_data);
    hids_init_obj.rep_map.p_data                 = rep_map_data;
    hids_init_obj.hid_information.bcd_hid        = BASE_USB_HID_SPEC_VERSION;
    hids_init_obj.hid_information.b_country_code = 0;
    hids_init_obj.hid_information.flags          = hid_info_flags;
    hids_init_obj.included_services_count        = 0;
    hids_init_obj.p_included_services_array      = NULL;

    hids_init_obj.rep_map.rd_sec         = SEC_JUST_WORKS;
    hids_init_obj.hid_information.rd_sec = SEC_JUST_WORKS;

    hids_init_obj.boot_mouse_inp_rep_sec.cccd_wr = SEC_JUST_WORKS;
    hids_init_obj.boot_mouse_inp_rep_sec.wr      = SEC_JUST_WORKS;
    hids_init_obj.boot_mouse_inp_rep_sec.rd      = SEC_JUST_WORKS;

    hids_init_obj.protocol_mode_rd_sec = SEC_JUST_WORKS;
    hids_init_obj.protocol_mode_wr_sec = SEC_JUST_WORKS;
    hids_init_obj.ctrl_point_wr_sec    = SEC_JUST_WORKS;

    err_code = ble_hids_init(&m_hids, &hids_init_obj);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing services that will be used by the application.
 */
static void services_init(void)
{
    qwr_init();
    dis_init();
    bas_init();
    hids_init();
}


/**@brief Function for initializing the battery sensor simulator.
 */
static void sensor_simulator_init(void)
{
    m_battery_sim_cfg.min          = MIN_BATTERY_LEVEL;
    m_battery_sim_cfg.max          = MAX_BATTERY_LEVEL;
    m_battery_sim_cfg.incr         = BATTERY_LEVEL_INCREMENT;
    m_battery_sim_cfg.start_at_max = true;

    sensorsim_init(&m_battery_sim_state, &m_battery_sim_cfg);
}


/**@brief Function for handling a Connection Parameters error.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    ret_code_t             err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAM_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = NULL;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for starting timers.
 */
static void timers_start(void)
{
    ret_code_t err_code;

    err_code = app_timer_start(m_battery_timer_id, BATTERY_LEVEL_MEAS_INTERVAL, NULL);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for putting the chip into sleep mode.
 *
 * @note This function will not return.
 */
static void sleep_mode_enter(void)
{
    ret_code_t err_code;

    err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    APP_ERROR_CHECK(err_code);

    // Prepare wakeup buttons.
    err_code = bsp_btn_ble_sleep_mode_prepare();
    APP_ERROR_CHECK(err_code);

    // Go to system-off mode (this function will not return; wakeup will cause a reset).
    err_code = sd_power_system_off();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling HID events.
 *
 * @details This function will be called for all HID events which are passed to the application.
 *
 * @param[in]   p_hids  HID service structure.
 * @param[in]   p_evt   Event received from the HID service.
 */
static void on_hids_evt(ble_hids_t * p_hids, ble_hids_evt_t * p_evt)
{
    switch (p_evt->evt_type)
    {
        case BLE_HIDS_EVT_BOOT_MODE_ENTERED:
            m_in_boot_mode = true;
            break;

        case BLE_HIDS_EVT_REPORT_MODE_ENTERED:
            m_in_boot_mode = false;
            break;

        case BLE_HIDS_EVT_NOTIF_ENABLED:
            break;

        default:
            // No implementation needed.
            break;
    }
}


/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    ret_code_t err_code;

    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_DIRECTED_HIGH_DUTY:
            NRF_LOG_INFO("Directed advertising.");
            err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING_DIRECTED);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_ADV_EVT_FAST:
            NRF_LOG_INFO("Fast advertising.");
            err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_ADV_EVT_SLOW:
            NRF_LOG_INFO("Slow advertising.");
#if SWIFT_PAIR_SUPPORTED == 1
            m_sp_advdata.p_manuf_specific_data = NULL;
            err_code = ble_advertising_advdata_update(&m_advertising, &m_sp_advdata, NULL);
            APP_ERROR_CHECK(err_code);
#endif
            err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING_SLOW);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_ADV_EVT_FAST_WHITELIST:
            NRF_LOG_INFO("Fast advertising with whitelist.");
            err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING_WHITELIST);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_ADV_EVT_SLOW_WHITELIST:
            NRF_LOG_INFO("Slow advertising with whitelist.");
            err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING_WHITELIST);
            APP_ERROR_CHECK(err_code);
            err_code = ble_advertising_restart_without_whitelist(&m_advertising);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_ADV_EVT_IDLE:
            err_code = bsp_indication_set(BSP_INDICATE_IDLE);
            APP_ERROR_CHECK(err_code);
            //sleep_mode_enter();
            break;

        case BLE_ADV_EVT_WHITELIST_REQUEST:
        {
            ble_gap_addr_t whitelist_addrs[BLE_GAP_WHITELIST_ADDR_MAX_COUNT];
            ble_gap_irk_t  whitelist_irks[BLE_GAP_WHITELIST_ADDR_MAX_COUNT];
            uint32_t       addr_cnt = BLE_GAP_WHITELIST_ADDR_MAX_COUNT;
            uint32_t       irk_cnt  = BLE_GAP_WHITELIST_ADDR_MAX_COUNT;

            err_code = pm_whitelist_get(whitelist_addrs, &addr_cnt,
                                        whitelist_irks,  &irk_cnt);
            APP_ERROR_CHECK(err_code);
            NRF_LOG_DEBUG("pm_whitelist_get returns %d addr in whitelist and %d irk whitelist",
                           addr_cnt,
                           irk_cnt);

            // Set the correct identities list (no excluding peers with no Central Address Resolution).
            identities_set(PM_PEER_ID_LIST_SKIP_NO_IRK);

            // Apply the whitelist.
            err_code = ble_advertising_whitelist_reply(&m_advertising,
                                                       whitelist_addrs,
                                                       addr_cnt,
                                                       whitelist_irks,
                                                       irk_cnt);
            APP_ERROR_CHECK(err_code);
        }
        break;

        case BLE_ADV_EVT_PEER_ADDR_REQUEST:
        {
            pm_peer_data_bonding_t peer_bonding_data;

            // Only Give peer address if we have a handle to the bonded peer.
            if (m_peer_id != PM_PEER_ID_INVALID)
            {

                err_code = pm_peer_data_bonding_load(m_peer_id, &peer_bonding_data);
                if (err_code != NRF_ERROR_NOT_FOUND)
                {
                    APP_ERROR_CHECK(err_code);

                    // Manipulate identities to exclude peers with no Central Address Resolution.
                    identities_set(PM_PEER_ID_LIST_SKIP_ALL);

                    ble_gap_addr_t * p_peer_addr = &(peer_bonding_data.peer_ble_id.id_addr_info);
                    err_code = ble_advertising_peer_addr_reply(&m_advertising, p_peer_addr);
                    APP_ERROR_CHECK(err_code);
                }

            }
            break;
        }

        default:
            break;
    }
}


/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    ret_code_t err_code;
    runcheck_led(GRN_2);
    switch (p_ble_evt->header.evt_id)
    {
        
        case BLE_GAP_EVT_CONNECTED:            
            
            NRF_LOG_INFO("Connected");
            err_code = bsp_indication_set(BSP_INDICATE_CONNECTED);
            APP_ERROR_CHECK(err_code);

            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;

            err_code = nrf_ble_qwr_conn_handle_assign(&m_qwr, m_conn_handle);
            APP_ERROR_CHECK(err_code);
            
            isLoop = true;
            break;

        case BLE_GAP_EVT_DISCONNECTED:            
            isLoop = false;
            // LED indication will be changed when advertising starts.

            m_conn_handle = BLE_CONN_HANDLE_INVALID;
            NRF_LOG_INFO("Disconnected");
            break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            isLoop = false;
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
            
        } break;

        case BLE_GATTC_EVT_TIMEOUT:
            isLoop = false;
            // Disconnect on GATT Client timeout event.
            NRF_LOG_DEBUG("GATT Client Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            isLoop = false;
            // Disconnect on GATT Server timeout event.
            NRF_LOG_DEBUG("GATT Server Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            
            break;
            

        default:
            // No implementation needed.
            break;
    }
}


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}


/**@brief Function for the Peer Manager initialization.
 */
static void peer_manager_init(void)
{
    ble_gap_sec_params_t sec_param;
    ret_code_t           err_code;

    err_code = pm_init();
    APP_ERROR_CHECK(err_code);

    memset(&sec_param, 0, sizeof(ble_gap_sec_params_t));

    // Security parameters to be used for all security procedures.
    sec_param.bond           = SEC_PARAM_BOND;
    sec_param.mitm           = SEC_PARAM_MITM;
    sec_param.lesc           = SEC_PARAM_LESC;
    sec_param.keypress       = SEC_PARAM_KEYPRESS;
    sec_param.io_caps        = SEC_PARAM_IO_CAPABILITIES;
    sec_param.oob            = SEC_PARAM_OOB;
    sec_param.min_key_size   = SEC_PARAM_MIN_KEY_SIZE;
    sec_param.max_key_size   = SEC_PARAM_MAX_KEY_SIZE;
    sec_param.kdist_own.enc  = 1;
    sec_param.kdist_own.id   = 1;
    sec_param.kdist_peer.enc = 1;
    sec_param.kdist_peer.id  = 1;

    err_code = pm_sec_params_set(&sec_param);
    APP_ERROR_CHECK(err_code);

    err_code = pm_register(pm_evt_handler);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the Advertising functionality.
 */
static void advertising_init(void)
{
    ret_code_t             err_code;
    uint8_t                adv_flags;
    ble_advertising_init_t init;

    memset(&init, 0, sizeof(init));

    adv_flags                            = BLE_GAP_ADV_FLAGS_LE_ONLY_LIMITED_DISC_MODE;
    init.advdata.name_type               = BLE_ADVDATA_FULL_NAME;
    init.advdata.include_appearance      = true;
    init.advdata.flags                   = adv_flags;
    init.advdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
    init.advdata.uuids_complete.p_uuids  = m_adv_uuids;
#if SWIFT_PAIR_SUPPORTED == 1
    init.advdata.p_manuf_specific_data = &m_sp_manuf_advdata;
    memcpy(&m_sp_advdata, &init.advdata, sizeof(m_sp_advdata));
#endif

    init.config.ble_adv_whitelist_enabled          = true;
    init.config.ble_adv_directed_high_duty_enabled = true;
    init.config.ble_adv_directed_enabled           = false;
    init.config.ble_adv_directed_interval          = 0;
    init.config.ble_adv_directed_timeout           = 0;
    init.config.ble_adv_fast_enabled               = true;
    init.config.ble_adv_fast_interval              = APP_ADV_FAST_INTERVAL;
    init.config.ble_adv_fast_timeout               = APP_ADV_FAST_DURATION;
    init.config.ble_adv_slow_enabled               = true;
    init.config.ble_adv_slow_interval              = APP_ADV_SLOW_INTERVAL;
    init.config.ble_adv_slow_timeout               = APP_ADV_SLOW_DURATION;

    init.evt_handler   = on_adv_evt;
    init.error_handler = ble_advertising_error_handler;

    err_code = ble_advertising_init(&m_advertising, &init);
    APP_ERROR_CHECK(err_code);

    ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);
}


/**@brief Function for the Event Scheduler initialization.
 */
static void scheduler_init(void)
{
    APP_SCHED_INIT(SCHED_MAX_EVENT_DATA_SIZE, SCHED_QUEUE_SIZE);
}


/**@brief Function for sending a Mouse Movement.
 *
 * @param[in]   x_delta   Horizontal movement.
 * @param[in]   y_delta   Vertical movement.
 */
static void mouse_movement_send(int16_t x_delta, int16_t y_delta)
{
    ret_code_t err_code;

    if (m_in_boot_mode)
    {
        x_delta = MIN(x_delta, 0x00ff);
        y_delta = MIN(y_delta, 0x00ff);

        err_code = ble_hids_boot_mouse_inp_rep_send(&m_hids, 0x00,(int8_t)x_delta,(int8_t)y_delta, 0,NULL, m_conn_handle);
    }
    else
    {
        uint8_t buffer[INPUT_REP_MOVEMENT_LEN];

        APP_ERROR_CHECK_BOOL(INPUT_REP_MOVEMENT_LEN == 3);

        x_delta = MIN(x_delta, 0x0fff);
        y_delta = MIN(y_delta, 0x0fff);

        buffer[0] = x_delta & 0x00ff;
        buffer[1] = ((y_delta & 0x000f) << 4) | ((x_delta & 0x0f00) >> 8);
        buffer[2] = (y_delta & 0x0ff0) >> 4;

        err_code = ble_hids_inp_rep_send(&m_hids,
                                         INPUT_REP_MOVEMENT_INDEX,
                                         INPUT_REP_MOVEMENT_LEN,
                                         buffer,
                                         m_conn_handle);
    }

    if ((err_code != NRF_SUCCESS) &&
        (err_code != NRF_ERROR_INVALID_STATE) &&
        (err_code != NRF_ERROR_RESOURCES) &&
        (err_code != NRF_ERROR_BUSY) &&
        (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING)
       )
    {
        APP_ERROR_HANDLER(err_code);
    }
}


/**@brief Function for handling events from the BSP module.
 *
 * @param[in]   event   Event generated by button press.
 */
static void bsp_event_handler(bsp_event_t event)
{
    ret_code_t err_code;

    switch (event)
    {
        case BSP_EVENT_SLEEP:
            //sleep_mode_enter();
            break;

        case BSP_EVENT_DISCONNECT:
            err_code = sd_ble_gap_disconnect(m_conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            if (err_code != NRF_ERROR_INVALID_STATE)
            {
                APP_ERROR_CHECK(err_code);
            }
            break;

        case BSP_EVENT_WHITELIST_OFF:
            if (m_conn_handle == BLE_CONN_HANDLE_INVALID)
            {
                err_code = ble_advertising_restart_without_whitelist(&m_advertising);
                if (err_code != NRF_ERROR_INVALID_STATE)
                {
                    APP_ERROR_CHECK(err_code);
                }
            }
            break;

        case BSP_EVENT_KEY_0:
            if (m_conn_handle != BLE_CONN_HANDLE_INVALID)
            {
                mouse_movement_send(-MOVEMENT_SPEED, 0);
            }
            break;

        case BSP_EVENT_KEY_1:
            if (m_conn_handle != BLE_CONN_HANDLE_INVALID)
            {
                mouse_movement_send(0, -MOVEMENT_SPEED);
            }
            break;

        case BSP_EVENT_KEY_2:
            if (m_conn_handle != BLE_CONN_HANDLE_INVALID)
            {
                mouse_movement_send(MOVEMENT_SPEED, 0);
            }
            break;

        case BSP_EVENT_KEY_3:
            if (m_conn_handle != BLE_CONN_HANDLE_INVALID)
            {
                mouse_movement_send(0, MOVEMENT_SPEED);
            }
            break;

        default:
            break;
    }
}


/**@brief Function for initializing buttons and leds.
 *
 * @param[out] p_erase_bonds  Will be true if the clear bonding button was pressed to wake the application up.
 */
static void buttons_leds_init(bool * p_erase_bonds)
{

}


/**@brief Function for initializing the nrf log module.
 */
static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}


/**@brief Function for initializing power management.
 */
static void power_management_init(void)
{
    ret_code_t err_code;
    err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the idle state (main loop).
 *
 * @details If there is no pending log operation, then sleep until next the next event occurs.
 */

uint8_t pct_addr = 0x33;


void tx(uint8_t reg, uint8_t val)
{
  ret_code_t err_code;

  uint8_t tx_buff[2] = {reg, val};
  err_code = nrf_drv_twi_tx(&m_twi, pct_addr, tx_buff, 2, false);
  if(err_code != NRF_SUCCESS)
  {
    LOG_temp("err_code:%d",err_code);
  }
}

uint8_t rx(uint8_t reg)
{  
  ret_code_t err_code;
  
  uint8_t tx_buff[1] = {reg};
  err_code = nrf_drv_twi_tx(&m_twi, pct_addr, tx_buff, 1, false);
  if(err_code != NRF_SUCCESS)
  {
    LOG_temp("err_code:%d",err_code);
  }

  
  uint8_t rx_buff[1];
  err_code = nrf_drv_twi_rx(&m_twi, pct_addr, rx_buff, 1);
  if(err_code != NRF_SUCCESS)
  {
    LOG_temp("err_code:%d",err_code);
  }
  return rx_buff[0];
}


void pct_software_reset()
{
  if(1)
  {//software reset flow
      LOG_temp("SOFTWARE RESET FLOW START");
      uint8_t rxbuf = 0;
      uint8_t loop = 100;

      tx(0x7C, 0x01); // disable deep sleep


      for(int i=0; i<loop; i++)
      {
        nrf_delay_ms(10);

        rxbuf = rx(0x7C); // check deep sleep awake 
        LOG_temp("<<[%d%d%d%d %d%d%d%d]",rxbuf>>7&0x01,rxbuf>>6&0x01,rxbuf>>5&0x01,rxbuf>>4&0x01,rxbuf>>3&0x01,rxbuf>>2&0x01,rxbuf>>1&0b00000001,rxbuf>>0&0x01);

        if((rxbuf & 0b00001000) == 0) //sleep_status[4] = 0
        {
          LOG_temp("sleep status : reset ok!");
          break;
        }
        else if(i == loop-1)
        {
          LOG_temp("sleep status check fail");
        }
      }

      tx(0x7B, 0x00); // enable host reset

      tx(0x7A, 0xAA); //write software reset code 1

      nrf_delay_ms(10);

      tx(0x7A, 0xBB); //write software reset code 2
      LOG_temp("SOFTWARE RESET FLOW DONE\n");
  }
}

void pct_boot_ready_check()
{

  if(1)
  {//boot ready chceck flow
    LOG_temp("BOOT READY CHECK START");
    uint8_t rxbuf = 0;
    uint8_t loop = 50;

    for(int i=0; i<loop; i++)
    {
      nrf_delay_ms(10);

      rxbuf = rx(0x70); //check boot ready status
      LOG_temp("<<[%d%d%d%d %d%d%d%d]",rxbuf>>7&0x01,rxbuf>>6&0x01,rxbuf>>5&0x01,rxbuf>>4&0x01,rxbuf>>3&0x01,rxbuf>>2&0x01,rxbuf>>1&0b00000001,rxbuf>>0&0x01);
      
      if((rxbuf & 0b00000001) != 0)//boot_ready_status[0] != 0
      {
        LOG_temp("boot ready state : ok!");
        break;
      }
      else if(i == loop-1)
      {
        LOG_temp("boot ready status check fail");
      }
    }
    LOG_temp("BOOT READY CHECK DONE\n");
  }
}

void pct_suspend()
{
  
  if(1)
  {// suspend flow
      LOG_temp("SUSPEND FLOW START");
      uint8_t rxbuf = 0;
      uint8_t loop = 100;

      tx(0x7C, 0x01); // disable deep sleep

      for(int i=0; i<loop; i++)
      {
        nrf_delay_ms(10);
        rxbuf = rx(0x7C); // check deep sleep awake 
        LOG_temp("<<[%d%d%d%d %d%d%d%d]",rxbuf>>7&0x01,rxbuf>>6&0x01,rxbuf>>5&0x01,rxbuf>>4&0x01,rxbuf>>3&0x01,rxbuf>>2&0x01,rxbuf>>1&0b00000001,rxbuf>>0&0x01);

        if((rxbuf & 0b00001000) == 0) //sleep_status[4] = 0
        {
          LOG_temp("sleep status : active -> suspend ok");
          break;
        }
        else if(i == loop-1)
        {
          LOG_temp("sleep status check fail");
        }
      }

      tx(0x7B, 0x01); // enable host reset

      tx(0x7A, 0x99); //write software reset code 1

      nrf_delay_ms(10);

      LOG_temp("SUSPEND FLOW DONE\n");
  }
}

void pct_resume()
{
    if(1)
  {// resume flow 
      LOG_temp("RESUME FLOW START");
      uint8_t rxbuf = 0;
      uint8_t loop = 50;

      tx(0x7A, 0xBB); // disable deep sleep

      for(int i=0; i<loop; i++)
      {
        nrf_delay_ms(10);
        rxbuf = rx(0x70); // check deep sleep awake 
        LOG_temp("<<[%d%d%d%d %d%d%d%d]",rxbuf>>7&0x01,rxbuf>>6&0x01,rxbuf>>5&0x01,rxbuf>>4&0x01,rxbuf>>3&0x01,rxbuf>>2&0x01,rxbuf>>1&0b00000001,rxbuf>>0&0x01);

        if((rxbuf & 0b00000001) == 0) //sleep_status[4] = 0
        {
          LOG_temp("sleep status: suspend -> run ok");
          break;
        }
        else if(i == loop-1)
        {
          LOG_temp("resume fail");
        }
      }
      nrf_delay_ms(10);

      LOG_temp("RESUME FLOW DONE\n");
  }

}

void dfu_get_bank()
{
  LOG_temp("GET BANK START");
  uint8_t rx_buff = 0;
  rx_buff = rx(0x7F);
  LOG_temp("<<[%d%d%d%d %d%d%d%d]",rx_buff>>7&0x01,rx_buff>>6&0x01,rx_buff>>5&0x01,rx_buff>>4&0x01,rx_buff>>3&0x01,rx_buff>>2&0x01,rx_buff>>1&0b00000001,rx_buff>>0&0x01);
  LOG_temp("GET BANK DONE\n");
}

void dfu_set_usr_bank()
{
  LOG_temp("SET BANK 0x06 START");
  
  tx(0x7F, 0x06);
    
  LOG_temp("SET BANK DONE\n");
}



bool pct_read_status()
{
  if(1)
  {//read status flow    
    LOG_temp("READ STATUS START");
    uint8_t rxbuf = 0;
    uint16_t loop = 4000;
    for(int i=0; i<loop; i++)
    {      
      rxbuf = rx(0x71); //check boot ready status
      LOG_temp("<<[%d%d%d%d %d%d%d%d]",rxbuf>>7&0x01,rxbuf>>6&0x01,rxbuf>>5&0x01,rxbuf>>4&0x01,rxbuf>>3&0x01,rxbuf>>2&0x01,rxbuf>>1&0b00000001,rxbuf>>0&0x01);
      
      if((rxbuf & 0b11111111) == 0)//boot_ready_status[0] != 0
      {
        LOG_temp("read status: retry");
        continue;
      }
      else
      {
        if((rxbuf & 0b00000001) != 0)//boot_ready_status[0] != 0
        {
          LOG_temp("read status: internal error");
          break;
        }
        if((rxbuf & 0b10000000) != 0)//boot_ready_status[0] != 0
        {
          LOG_temp("read status: watchdog error");
          break;
        }
        else
        {
          LOG_temp("read status: ok");          
          return true;
        }
      }
      
      if(i == loop-1)
      {
        LOG_temp("read status check fail");
        break;
      }
    }
    LOG_temp("READ STATUS FLOW DONE\n");
    return false;
  }
}


//9 : finger 1011:x 1213:y
void pct_report_access(uint8_t* result, uint8_t length)
{
    if(1)
    {//report access flow
      LOG_temp("REPORT ACCESS FLOW START");
      const uint16_t loop = length;
      //uint8_t rx_buff[length] = {0,};
      memset(result, 0x00, length);

      tx(0x09, 0x08);
      tx(0x0A, 0x00);

      for(uint16_t i=0; i<loop; i++)
      {
        result[i] = rx(0x0B);
      }
      //LOG_pos("<<%d[0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x]",rx_buff[8],rx_buff[0],rx_buff[1],rx_buff[2],rx_buff[3],rx_buff[4],rx_buff[5],rx_buff[6],rx_buff[7],rx_buff[8],rx_buff[9],rx_buff[10],rx_buff[11],rx_buff[12],rx_buff[13],rx_buff[14],rx_buff[15],rx_buff[16],rx_buff[17]);  

      tx(0x0A,0x01);

      LOG_temp("REPORT ACCESS FLOW DONE\n");
      
    }
}

void pct_ack_and_clear_int()
{
    if(1)
    {//ack and clear interrupt
      LOG_temp("ACK AND CLEAR INT START");
      tx(0x71,0x00);
      LOG_temp("ACK AND CLEAR INT DONE\n");
    }
}


void mkb_event_process()
{    
        
}





static void idle_state_handle(void)
{        
    app_sched_execute();
    if (NRF_LOG_PROCESS() == false)
    {
        nrf_pwr_mgmt_run();
    }
}



void runcheck_led(uint8_t led)
{
  static uint8_t val=0;
  val++;

  if(val%2)
  {
    nrf_gpio_pin_write(led,0);
  }
  else
  {
    nrf_gpio_pin_write(led,1);
  }
}




void mkb_ble_movement_send(int16_t x_delta, int16_t y_delta)
{
    ret_code_t err_code;


    uint8_t buffer[INPUT_REP_MOVEMENT_LEN];

    APP_ERROR_CHECK_BOOL(INPUT_REP_MOVEMENT_LEN == 3);


    //LOG_pos("1. %d, %d", x_delta,y_delta);

    x_delta = MIN(x_delta, 0x0fff);
    y_delta = MIN(y_delta, 0x0fff);    

    buffer[0] = x_delta & 0x00ff;
    buffer[1] = ((y_delta & 0x000f) << 4) | ((x_delta & 0x0f00) >> 8);
    buffer[2] = (y_delta & 0x0ff0) >> 4;
    
    //LOG_pos("2. %d, %d, %d", buffer[0],buffer[1], buffer[2]);
    //LOG_pos("x:%d, y:%d", x_delta, y_delta);
    
    err_code = ble_hids_inp_rep_send(&m_hids,
                                        INPUT_REP_MOVEMENT_INDEX,
                                        INPUT_REP_MOVEMENT_LEN,
                                        buffer,
                                        m_conn_handle);


    if ((err_code != NRF_SUCCESS) &&
        (err_code != NRF_ERROR_INVALID_STATE) &&
        (err_code != NRF_ERROR_RESOURCES) &&
        (err_code != NRF_ERROR_BUSY) &&
        (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING) &&
        (err_code != NRF_ERROR_FORBIDDEN) &&
        (err_code != BLE_ERROR_INVALID_CONN_HANDLE) // 0x3002
       )
    {
        
        //MKB_ERROR_HANDLER(err_code);
    }
}




void check_handler()
{
    
    if(pct_read_status())
    {
      static int16_t oldx = 0;
      static int16_t oldy = 0;

      int16_t x = 0;
      int16_t y = 0;

      int16_t deltax;
      int16_t deltay;


      runcheck_led(RED_3);
      uint8_t touch_raw[18];
    
        pct_report_access(touch_raw,18);
        
        pixart_touch_packet_t* packet;
        
        memset(packet,touch_raw, 18*sizeof(uint8_t));


      x = (touch_raw[10]<<8)+ (touch_raw[11])+10000;
      y = (touch_raw[12]<<8)+ (touch_raw[13])+10000;

      deltax = x - oldx;
      deltay = y - oldy;

      oldx = x;
      oldy = y;

            
      //LOG_pos("%d, x:%d, y:%d", touch_raw[8], deltax, deltay);
      LOG_pos("%d, x:%d, y:%d", touch_raw[8], packet->info[1].x, packet->info[1].y);

      
        if(abs(deltax) > 40)
        {
            deltax = 0;
        }
        if(abs(deltay)>40)
        {
            deltay=0;
        }

      mouse_movement_send(deltax,deltay);

      pct_ack_and_clear_int();
                
      
    }


}




void check()
{
    if(isLoop)
    {
        if(app_sched_queue_space_get() > 5)
        {
            app_sched_event_put(NULL, 0, check_handler);
        }
    }
  
}




//app_sched_event_put(NULL, 0, mkb_event_process);

int main(void)
{
    bool erase_bonds;

    // Initialize.
    log_init();
    timers_init();    
    

    buttons_leds_init(&erase_bonds);
    
    power_management_init();
    ble_stack_init();
    scheduler_init();
    
    led_init();
    twi_init();

    gap_params_init();
    gatt_init();
    advertising_init();
    services_init();
    sensor_simulator_init();
    conn_params_init();
    peer_manager_init();


    // Start execution.
    NRF_LOG_INFO("HID Mouse example started.");
    timers_start();

    delete_bonds();
    ble_advertising_restart_without_whitelist(&m_advertising);
    
    //advertising_start(erase_bonds);

    pct_software_reset();
    pct_boot_ready_check();
    //pct_suspend();
    //pct_resume();
    //pct_boot_ready_check();
    
    runcheck_led(RED_1);
    nrf_delay_ms(100);


    app_timer_start(m_handler_timer_id, 16, NULL);

    
    

    
    

    // Enter main loop.
    for (;;)
    {                
        idle_state_handle();
    }
}



/**
 * @}
 */
