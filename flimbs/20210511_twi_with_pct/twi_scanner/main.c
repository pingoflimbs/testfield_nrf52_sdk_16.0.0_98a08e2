/**
 * Copyright (c) 2016 - 2019, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/** @file
 * @defgroup tw_scanner main.c
 * @{
 * @ingroup nrf_twi_example
 * @brief TWI Sensor Example main file.
 *
 * This file contains the source code for a sample application using TWI.
 *
 */



#include <stdio.h>
#include "boards.h"
#include "app_util_platform.h"
#include "app_error.h"
#include "nrf_drv_twi.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"


//#include "m_pixart_dfu.h"
#include "m_pixart_dfu_mkb420_0505_2.h"
//#include "m_pixart_dfu_ud3n_0426_swc.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "SEGGER_RTT.h"





/* LOG defines */
#define __FILENAME__ (strchr(__FILE__, '\\') ? strrchr(__FILE__,'\\')+1 : strrchr(__FILE__,'/')+1) // string.h
#define FLIMBS_LOG(fmt,...)\
do{\
    SEGGER_RTT_SetTerminal(0);\
    char dbg_str[256];\
    sprintf(dbg_str, "[%4d:%14.14s:%-18.18s:%04d] " fmt "\n", 0, __FILENAME__, __func__, __LINE__, ##__VA_ARGS__);\
    SEGGER_RTT_printf(0,"E:%s",dbg_str);\
} while(0)



#if 1
#define LOG_boot_ready_check(fmt,...) FLIMBS_LOG(fmt,##__VA_ARGS__)
#define LOG_boot_check_result(fmt,...) FLIMBS_LOG(fmt,##__VA_ARGS__)
#define LOG_status_check(fmt,...) FLIMBS_LOG(fmt,##__VA_ARGS__)
#define LOG_state_check_result(fmt,...) FLIMBS_LOG(fmt,##__VA_ARGS__)
#define LOG_ack_and_clear(fmt,...) FLIMBS_LOG(fmt,##__VA_ARGS__)
#define LOG_read_report(fmt,...) FLIMBS_LOG(fmt,##__VA_ARGS__)
#define LOG_read_report_result(fmt,...) FLIMBS_LOG(fmt,##__VA_ARGS__)
#define LOG_finger_cnt(fmt,...) FLIMBS_LOG(fmt,##__VA_ARGS__)
#define LOG_software_reset(fmt,...) FLIMBS_LOG(fmt,##__VA_ARGS__)
#define LOG_tx(fmt,...) FLIMBS_LOG(fmt,##__VA_ARGS__)


#define LOG_system_init(fmt,...) FLIMBS_LOG(fmt,##__VA_ARGS__)
#define LOG_error(fmt,...) FLIMBS_LOG(fmt,##__VA_ARGS__)
#define LOG_temp(fmt,...) FLIMBS_LOG(fmt,##__VA_ARGS__)
#define LOG_pos(fmt,...) FLIMBS_LOG(fmt,##__VA_ARGS__)
#define LOG_hex_dbg(fmt,...) //FLIMBS_LOG(fmt,##__VA_ARGS__)
#define LOG_prjid_version(fmt,...) FLIMBS_LOG(fmt,##__VA_ARGS__)

#else
#define LOG_boot_ready_check(fmt,...) //FLIMBS_LOG(fmt,##__VA_ARGS__)
#define LOG_boot_check_result(fmt,...) FLIMBS_LOG(fmt,##__VA_ARGS__)

#define LOG_status_check(fmt,...) //FLIMBS_LOG(fmt,##__VA_ARGS__)
#define LOG_state_check_result(fmt,...) FLIMBS_LOG(fmt,##__VA_ARGS__)

#define LOG_ack_and_clear(fmt,...) //FLIMBS_LOG(fmt,##__VA_ARGS__)

#define LOG_read_report(fmt,...) //FLIMBS_LOG(fmt,##__VA_ARGS__)
#define LOG_read_report_result(fmt,...) FLIMBS_LOG(fmt,##__VA_ARGS__)
#define LOG_finger_cnt(fmt,...) //FLIMBS_LOG(fmt,##__VA_ARGS__)
#endif

//custom log







/* TWI instance ID. */
#if TWI0_ENABLED
#define TWI_INSTANCE_ID     0
#elif TWI1_ENABLED
#define TWI_INSTANCE_ID     1
#endif


/* Number of possible TWI addresses. */
#define TWI_ADDRESSES      127

#define SCL 2
#define SDA 47
#define INT 45
#define RED_1 9
#define GRN_1 10
#define BLU_1 42
#define RED_2 26
#define GRN_2 4
#define BLU_2 6
#define RED_3 8
#define GRN_3 41
#define BLU_3 12
#define RED_4 34
#define GRN_4 36
#define BLU_4 38

#define BANK_USER 0x06

#define REG_GET_REPORT1 0x08
#define REG_GET_REPORT2 0x00
#define REG_READ_REPORT 0x0B

#define REG_BOOT_READY 0x70
#define REG_STATUS 0x71
#define REG_SLEEP_STATUS 0x7C


#define REG_HOST_RESET 0x7A
#define REG_HOST_RESET_ENABLE 0x7B
#define REG_DEEPSLEEP 0x7C

#define REG_CHANGEBANK 0x7F

#define REG_INDIRECT_BANK 0x73
#define REG_INDIRECT_BANK_OFFSET 0x80
#define REG_INDIRECT_REG 0x74
#define REG_INDIRECT_DATA 0x75

/* TWI instance. */
static const nrf_drv_twi_t m_twi = NRF_DRV_TWI_INSTANCE(TWI_INSTANCE_ID);
static uint8_t prj_version_old[] = {0xFF,0xFF};
/**
 * @brief TWI initialization.
 */
void twi_init (void)
{
    ret_code_t err_code;

    const nrf_drv_twi_config_t twi_config = {
       .scl                = 2,
       .sda                = 47,
       .frequency          = NRF_DRV_TWI_FREQ_100K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH,
       .clear_bus_init     = false
    };

    err_code = nrf_drv_twi_init(&m_twi, &twi_config, NULL, NULL);
    APP_ERROR_CHECK(err_code);

    nrf_drv_twi_enable(&m_twi);
}



void led_init()
{

  nrf_gpio_pin_dir_set(RED_1,NRF_GPIO_PIN_DIR_OUTPUT);
  nrf_gpio_pin_dir_set(GRN_1,NRF_GPIO_PIN_DIR_OUTPUT);
  nrf_gpio_pin_dir_set(BLU_1,NRF_GPIO_PIN_DIR_OUTPUT);

  nrf_gpio_pin_dir_set(RED_2,NRF_GPIO_PIN_DIR_OUTPUT);
  nrf_gpio_pin_dir_set(GRN_2,NRF_GPIO_PIN_DIR_OUTPUT);
  nrf_gpio_pin_dir_set(BLU_2,NRF_GPIO_PIN_DIR_OUTPUT);


  nrf_gpio_pin_dir_set(RED_3,NRF_GPIO_PIN_DIR_OUTPUT);
  nrf_gpio_pin_dir_set(GRN_3,NRF_GPIO_PIN_DIR_OUTPUT);
  nrf_gpio_pin_dir_set(BLU_3,NRF_GPIO_PIN_DIR_OUTPUT);


  nrf_gpio_pin_dir_set(RED_4,NRF_GPIO_PIN_DIR_OUTPUT);
  nrf_gpio_pin_dir_set(GRN_4,NRF_GPIO_PIN_DIR_OUTPUT);
  nrf_gpio_pin_dir_set(BLU_4,NRF_GPIO_PIN_DIR_OUTPUT);

}

void led_dbg_on(uint8_t wave_cnt)
{  
  for(int i=0; i < wave_cnt; i++)
  {    
    nrf_gpio_pin_write(BLU_1, 1);
    nrf_delay_us(100*(i+1));
    
    nrf_gpio_pin_write(BLU_1, 0);
    nrf_delay_us(100);
  }
  nrf_gpio_pin_write(BLU_1, 1);  
  nrf_delay_us(800);
}
void led_dbg_off(uint8_t wave_cnt)
{
  for(int i = wave_cnt; i > 0; i--)
  { 
    nrf_gpio_pin_write(BLU_1, 0);
    nrf_delay_us(100);
    

    nrf_gpio_pin_write(BLU_1, 1);
    nrf_delay_us(100*(i+1));
    
  }    
  nrf_gpio_pin_write(BLU_1, 0);
  nrf_delay_ms(2);
}


/**
 * @brief Function for main application entry.
 */


uint8_t pct_addr = 0x33;
ret_code_t err_code;

#define tx_buff_len 8
#define rx_buff_len 127

uint8_t tx_buff[tx_buff_len];
uint8_t rx_buff[rx_buff_len];

void scan_address()
{
    LOG_system_init("TWI scanner started.");
    ret_code_t err_code;
    uint8_t address;
    uint8_t sample_data;
    bool detected_device = false;

    for (address = 1; address <= TWI_ADDRESSES; address++)
    {
        err_code = nrf_drv_twi_rx(&m_twi, address, &sample_data, sizeof(sample_data));
        if (err_code == NRF_SUCCESS)
        {
            detected_device = true;
            LOG_system_init("TWI device detected at address 0x%02x.", address);
        }
    }

    if (!detected_device)
    {
        LOG_system_init("No device was found.");
    }
    LOG_system_init("TWI scanner done.\n");
}

void tx(uint8_t reg, uint8_t val)
{
  ret_code_t err_code;

  uint8_t tx_buff[2] = {reg, val};
  err_code = nrf_drv_twi_tx(&m_twi, pct_addr, tx_buff, 2, false);
  if(err_code != NRF_SUCCESS)
  {
    LOG_error("err_code:%d",err_code);
  }
}

uint8_t rx(uint8_t reg)
{
  ret_code_t err_code;

  uint8_t tx_buff[1] = {reg};
  err_code = nrf_drv_twi_tx(&m_twi, pct_addr, tx_buff, 1, false);
  if(err_code != NRF_SUCCESS)
  {
    LOG_error("err_code:%d",err_code);
  }

  nrf_delay_ms(1);
  uint8_t rx_buff[1];
  err_code = nrf_drv_twi_rx(&m_twi, pct_addr, rx_buff, 1);
  if(err_code != NRF_SUCCESS)
  {
    LOG_error("err_code:%d",err_code);
  }
  return rx_buff[0];
}


void tx_dfu(uint8_t bank, uint8_t reg, uint8_t data)
{
  tx(0x7F, bank);
  tx(reg, data);
}

uint8_t rx_dfu(uint8_t bank, uint8_t reg, bool direct)
{
  if(direct == true)
  {
    tx(0x7F, bank);
    return rx(reg);
  }
  else
  {
    tx(0x7F, 0x06);
    tx(0x73, 0x80 + bank);
    tx(0x74, reg);
    return rx(0x75);
  }
}

void tx_usr(uint8_t bank, uint8_t reg, uint8_t data)
{
  tx(0x7F, 0x06);
  tx(0x73, bank);
  tx(0x74, reg);
  tx(0x75, data);
}

uint8_t rx_usr(uint8_t bank, uint8_t reg)
{
  tx(0x7F, 0x06);
  tx(0x73, bank);
  tx(0x74, reg);
  return rx(0x75);
}


void pct_software_reset()
{
  if(1)
  {//software reset flow
      LOG_temp("SOFTWARE RESET FLOW START");
      uint8_t rxbuf = 0;
      uint8_t loop = 100;

      tx(0x7C, 0x01); // disable deep sleep


      for(int i=0; i<loop; i++)
      {
        nrf_delay_ms(10);

        rxbuf = rx(0x7C); // check deep sleep awake 
        LOG_temp("<<[%d%d%d%d %d%d%d%d]",rxbuf>>7&0x01,rxbuf>>6&0x01,rxbuf>>5&0x01,rxbuf>>4&0x01,rxbuf>>3&0x01,rxbuf>>2&0x01,rxbuf>>1&0b00000001,rxbuf>>0&0x01);

        if((rxbuf & 0b00001000) == 0) //sleep_status[4] = 0
        {
          LOG_temp("sleep status : reset ok!");
          break;
        }
        else if(i == loop-1)
        {
          LOG_temp("sleep status check fail");
        }
      }

      tx(0x7B, 0x00); // enable host reset

      tx(0x7A, 0xAA); //write software reset code 1

      nrf_delay_ms(10);

      tx(0x7A, 0xBB); //write software reset code 2
      LOG_temp("SOFTWARE RESET FLOW DONE\n");
  }
}

void pct_boot_ready_check()
{

  if(1)
  {//boot ready chceck flow
    LOG_temp("BOOT READY CHECK START");
    uint8_t rxbuf = 0;
    uint8_t loop = 50;

    for(int i=0; i<loop; i++)
    {
      nrf_delay_ms(10);

      rxbuf = rx(0x70); //check boot ready status
      LOG_temp("<<[%d%d%d%d %d%d%d%d]",rxbuf>>7&0x01,rxbuf>>6&0x01,rxbuf>>5&0x01,rxbuf>>4&0x01,rxbuf>>3&0x01,rxbuf>>2&0x01,rxbuf>>1&0b00000001,rxbuf>>0&0x01);
      
      if((rxbuf & 0b00000001) != 0)//boot_ready_status[0] != 0
      {
        LOG_temp("boot ready state : ok!");
        break;
      }
      else if(i == loop-1)
      {
        LOG_temp("boot ready status check fail");
      }
    }
    LOG_temp("BOOT READY CHECK DONE\n");
  }
}

void pct_suspend()
{
  
  if(1)
  {// suspend flow
      LOG_temp("SUSPEND FLOW START");
      uint8_t rxbuf = 0;
      uint8_t loop = 100;

      tx(0x7C, 0x01); // disable deep sleep

      for(int i=0; i<loop; i++)
      {
        nrf_delay_ms(10);
        rxbuf = rx(0x7C); // check deep sleep awake 
        LOG_temp("<<[%d%d%d%d %d%d%d%d]",rxbuf>>7&0x01,rxbuf>>6&0x01,rxbuf>>5&0x01,rxbuf>>4&0x01,rxbuf>>3&0x01,rxbuf>>2&0x01,rxbuf>>1&0b00000001,rxbuf>>0&0x01);

        if((rxbuf & 0b00001000) == 0) //sleep_status[4] = 0
        {
          LOG_temp("sleep status : active -> suspend ok");
          break;
        }
        else if(i == loop-1)
        {
          LOG_temp("sleep status check fail");
        }
      }

      tx(0x7B, 0x01); // enable host reset

      tx(0x7A, 0x99); //write software reset code 1

      nrf_delay_ms(10);

      LOG_temp("SUSPEND FLOW DONE\n");
  }
}

void pct_resume()
{
    if(1)
  {// resume flow 
      LOG_temp("RESUME FLOW START");
      uint8_t rxbuf = 0;
      uint8_t loop = 50;

      tx(0x7A, 0xBB); // disable deep sleep

      for(int i=0; i<loop; i++)
      {
        nrf_delay_ms(10);
        rxbuf = rx(0x70); // check deep sleep awake 
        LOG_temp("<<[%d%d%d%d %d%d%d%d]",rxbuf>>7&0x01,rxbuf>>6&0x01,rxbuf>>5&0x01,rxbuf>>4&0x01,rxbuf>>3&0x01,rxbuf>>2&0x01,rxbuf>>1&0b00000001,rxbuf>>0&0x01);

        if((rxbuf & 0b00000001) == 0) //sleep_status[4] = 0
        {
          LOG_temp("sleep status: suspend -> run ok");
          break;
        }
        else if(i == loop-1)
        {
          LOG_temp("resume fail");
        }
      }
      nrf_delay_ms(10);

      LOG_temp("RESUME FLOW DONE\n");
  }

}

void dfu_get_bank()
{
  LOG_temp("GET BANK START");
  uint8_t rx_buff = 0;
  rx_buff = rx(0x7F);
  LOG_temp("<<[%d%d%d%d %d%d%d%d]",rx_buff>>7&0x01,rx_buff>>6&0x01,rx_buff>>5&0x01,rx_buff>>4&0x01,rx_buff>>3&0x01,rx_buff>>2&0x01,rx_buff>>1&0b00000001,rx_buff>>0&0x01);
  LOG_temp("GET BANK DONE\n");
}

void dfu_set_usr_bank()
{
  LOG_temp("SET BANK 0x06 START");
  
  tx(0x7F, 0x06);
    
  LOG_temp("SET BANK DONE\n");
}

uint8_t pct_get_version_and_projectid()
{
    if(1)
    {//get version
      LOG_temp("GET PID AND VER START");
      uint8_t rx_buff_ver = 0;
      uint8_t rx_buff_prj = 0;
      
      rx_buff_prj = rx_usr(0x00, 0xB3);      
      
      rx_buff_ver = rx_usr(0x00, 0xB2);
      LOG_prjid_version("<<<<<<  prj:0x%02x, ver:0x%02x -> prj:0x%02x, ver:0x%02x   >>>>>>",prj_version_old[0],prj_version_old[1],rx_buff_prj, rx_buff_ver);
      prj_version_old[0] = rx_buff_prj;
      prj_version_old[1] = rx_buff_ver;
      LOG_temp("GET PID AND VER DONE\n");      
      
    }
}


void pct_read_status()
{
  if(1)
  {//read status flow
    LOG_temp("READ STATUS START");
    uint8_t rxbuf = 0;
    uint16_t loop = 4000;
    for(int i=0; i<loop; i++)
    {
      nrf_delay_ms(1);
      rxbuf = rx(0x71); //check boot ready status
      LOG_temp("<<[%d%d%d%d %d%d%d%d]",rxbuf>>7&0x01,rxbuf>>6&0x01,rxbuf>>5&0x01,rxbuf>>4&0x01,rxbuf>>3&0x01,rxbuf>>2&0x01,rxbuf>>1&0b00000001,rxbuf>>0&0x01);
      
      if((rxbuf & 0b11111111) == 0)//boot_ready_status[0] != 0
      {
        LOG_temp("read status: retry");
        continue;
      }
      else
      {
        if((rxbuf & 0b00000001) != 0)//boot_ready_status[0] != 0
        {
          LOG_temp("read status: internal error");
          break;
        }
        if((rxbuf & 0b10000000) != 0)//boot_ready_status[0] != 0
        {
          LOG_temp("read status: watchdog error");
          break;
        }
        else
        {
          LOG_temp("read status: ok");
          break;
        }
      }
      
      if(i == loop-1)
      {
        LOG_temp("read status check fail");
        break;
      }
    }
    LOG_temp("READ STATUS FLOW DONE\n");
  }
}


void pct_report_access()
{
    if(1)
    {//report access flow
      LOG_temp("REPORT ACCESS FLOW START");
      const uint16_t loop = 9+9*1;
      uint8_t rx_buff[9+9*1] = {0,};

      tx(0x09, 0x08);
      tx(0x0A, 0x00);

      for(uint16_t i=0; i<loop; i++)
      {
        rx_buff[i] = rx(0x0B);
      }
      LOG_pos("<<%d[0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x]",rx_buff[8],rx_buff[0],rx_buff[1],rx_buff[2],rx_buff[3],rx_buff[4],rx_buff[5],rx_buff[6],rx_buff[7],rx_buff[8],rx_buff[9],rx_buff[10],rx_buff[11],rx_buff[12],rx_buff[13],rx_buff[14],rx_buff[15],rx_buff[16],rx_buff[17]);  

      tx(0x0A,0x01);

      LOG_temp("REPORT ACCESS FLOW DONE\n");
    }
}

void pct_ack_and_clear_int()
{
    if(1)
    {//ack and clear interrupt
      LOG_temp("ACK AND CLEAR INT START");
      tx(0x71,0x00);
      LOG_temp("ACK AND CLEAR INT DONE\n");
    }
}


void dfu_power_up_flash()
{
  if(1)
  {// power up flash controller
    LOG_temp("POWER UP FLASH CONTROLLER START"); 
    tx_dfu(0x01, 0x0D, 0x02); // flash controller ON
    LOG_temp("POWER UP FLASH CONTROLLER DONE\n"); 
  }

}

void dfu_enter_engineering_mode()
{
  if(1)
  {//Enter Engineering Mode
    LOG_temp("ENTER ENGINEERING MODE START");
    tx_dfu(0x01, 0x2C, 0xAA);
    tx_dfu(0x01, 0x2D, 0xCC);
    nrf_delay_ms(10);
    LOG_temp("ENTER ENGINEERING MODE DONE\n");
  }
  
}

void dfu_exit_enginerring_mode()
{
  if(1)
  {//Exit Engineering Mode
    LOG_temp("EXIT ENGINEERING MODE START");
    uint8_t rx_buff = 0;
    uint8_t loop = 50;
    ret_code_t err_code;
    

    tx_dfu(0x01, 0x2C, 0xAA);
    tx_dfu(0x01, 0x2D, 0xBB);

    for(int i=0; i<loop; i++)
    {
      
      nrf_delay_ms(10);
      rx_buff = rx_dfu(0x06, 0x70, 1); // boot ready check
      LOG_temp("<<[%d%d%d%d %d%d%d%d]",rx_buff>>7&0x01,rx_buff>>6&0x01,rx_buff>>5&0x01,rx_buff>>4&0x01,rx_buff>>3&0x01,rx_buff>>2&0x01,rx_buff>>1&0b00000001,rx_buff>>0&0x01);
      if((rx_buff & 0x00000001) != 0)
      {
        LOG_temp("boot ready : ok");
        break;
      }

      if(i == loop - 1)
      {
        LOG_temp("boot ready check failed");
      }
    }

    LOG_temp("EXIT ENGINEERING MODE DONE\n");
  }

}

void dfu_flash_execute(uint16_t inst_cmd, uint32_t ccr_cmd, uint16_t data_cnt)
{
  if(1)
  {
    LOG_temp("FLASH EXECUTE START");
    uint16_t frame_start;
    uint16_t check_cnt;
    uint16_t MAX_CHECK_CNT = 10;

    tx_dfu(0x04, 0x2C, inst_cmd); // write intersram access

    // Write CCR command
    tx_dfu(0x04, 0x40, (uint8_t)((ccr_cmd >> 0) & 0xFF));
    tx_dfu(0x04, 0x41, (uint8_t)((ccr_cmd >> 8) & 0xFF));
    tx_dfu(0x04, 0x42, (uint8_t)((ccr_cmd >> 16) & 0xFF));
    tx_dfu(0x04, 0x43, (uint8_t)((ccr_cmd >> 24) & 0xFF));

    // Write data count
    tx_dfu(0x04, 0x44, (uint8_t)((data_cnt >> 0) & 0xFF));
    tx_dfu(0x04, 0x45, (uint8_t)((data_cnt >> 8) & 0xFF));

    tx_dfu(0x04, 0x56, 0x01); // Frame start

    for (check_cnt = 0; check_cnt < MAX_CHECK_CNT; check_cnt++)
    {
        nrf_delay_ms(1);
        frame_start = rx_dfu(0x04, 0x56, false);// indirectly read frame_start register

        if (frame_start == 0)
        {
          break;
        }
        else if (check_cnt == MAX_CHECK_CNT - 1)
        {
            LOG_temp("flash execute : error");
        }
    }

    LOG_temp("FLASH EXECUTE DONE \n");
  }
}

void dfu_flash_write_enable()
{
  if(1)
  {
    LOG_temp("FLASH WRITE ENABLE START");
    uint8_t flash_status;
    uint16_t check_cnt;
    uint16_t MAX_CHECK_CNT = 10;

    dfu_flash_execute(0x00, 0x00000106, 0); // WEN

    for (check_cnt = 0; check_cnt < MAX_CHECK_CNT; check_cnt++)
    {
        dfu_flash_execute(0x01, 0x01000105, 1); // RDSR
        nrf_delay_ms(1);
        flash_status = rx_dfu(0x04, 0x1C, 0); // indirectly read flash_status register

        if ((flash_status & 0x02) == 0x02)
        {
            break;
        }
        else if (check_cnt == MAX_CHECK_CNT)
        {
            LOG_temp("flash write enable : error");
        }
    }
  
  LOG_temp("FLASH WRITE ENABLE DONE \n");
  }
}

void dfu_wait_flash_busy()
{
  if(1)
  {
    LOG_temp("WAIT FLASH BUSY START");

    uint8_t flash_status;
    uint16_t check_cnt;
    uint16_t MAX_CHECK_CNT = 1000;

    for (check_cnt = 0; check_cnt < MAX_CHECK_CNT; check_cnt++)
    {
        dfu_flash_execute(0x01, 0x01000105, 1); // RDSR
        nrf_delay_ms(1);
        flash_status = rx_dfu(0x04, 0x1C, false); // indirectly read flash_status register

        if ((flash_status & 0x01) == 0x00) // check if bit 0 is equal to 0
        {
            break;
        }
        else if (check_cnt == MAX_CHECK_CNT - 1)
        {
            LOG_temp("wait flash busy : error");
        }
        else
        {
          LOG_temp("wait flash busy : busy");
        }
    }
    LOG_temp("WAIT FLASH BUSY DONE\n");
  }
}

void dfu_erase_flash_sector(uint16_t sector)
{
    if(1)
    {//dfu erase flash secotr
      LOG_temp("ERASE FLASH SECTOR START");
            
      uint32_t flash_addr = (uint32_t)(sector)*4096;
      dfu_wait_flash_busy();
      
      tx_dfu(0x04, 0x48, (uint8_t)((flash_addr >> 0) & 0xff));
      tx_dfu(0x04, 0x49, (uint8_t)((flash_addr >> 8) & 0xff));
      tx_dfu(0x04, 0x4a, (uint8_t)((flash_addr >> 16) & 0xff));
      tx_dfu(0x04, 0x4b, (uint8_t)((flash_addr >> 24) & 0xff));
      dfu_flash_execute(0x00, 0x00002520, 0);

      LOG_temp("ERASE FLASH SECTOR DONE\n");
    }
}

void dfu_write_256B_data_to_sram(uint8_t* data, uint16_t data_size, uint16_t offset)
{
    if(1)
    {//write 256B data to sram
      LOG_temp("WRITE 256B DATA TO SRAM START");

      uint16_t data_count;
      uint8_t write_data;
      tx_dfu(0x06, 0x09, 0x08); // Set SRAM select
      tx_dfu(0x06, 0x0a, 0x00); // Set SRAM NCS to 0

      
      for (data_count = 0; data_count < 256; data_count++)
      {
          write_data = (offset + data_count < data_size) ? data[offset + data_count] : (uint8_t)0xff;
          tx_dfu(0x06, 0x0b, write_data);          
          LOG_hex_dbg("0x%02x",write_data);
      }



      tx_dfu(0x06, 0x0a, 0x01); // Set SRAM NCS to 1

      LOG_temp("WRITE 256B DATA TO SRAM DONE\n");
    }
}

void dfu_program_256B_sram_data_to_flash(uint32_t sector, uint32_t page)
{
    if(1)
    {//program 26B srma data to flash
      LOG_temp("PROGRAM 256B SRAM DATA TO FLASH START");

      uint32_t flash_addr;

      dfu_wait_flash_busy();
      dfu_flash_write_enable();

      // Set flash address
      flash_addr = (uint32_t)sector * 4096 + (uint32_t)page * 256;
      tx_dfu(0x04, 0x48, (uint8_t)((flash_addr >> 0) & 0xff));
      tx_dfu(0x04, 0x49, (uint8_t)((flash_addr >> 8) & 0xff));
      tx_dfu(0x04, 0x4a, (uint8_t)((flash_addr >> 16) & 0xff));
      tx_dfu(0x04, 0x4b, (uint8_t)((flash_addr >> 24) & 0xff));

      // Set SRAM access offset
      tx_dfu(0x04, 0x2e, 0x00);
      tx_dfu(0x04, 0x2f, 0x00);

      dfu_flash_execute(0x84, 0x01002502, 256); // Flash Execute with Page Program

      LOG_temp("PROGRAM 256B SRAM DATA TO FLASH DONE\n");
    }
}



int main(void)
{
  APP_ERROR_CHECK(NRF_LOG_INIT(NULL));
  NRF_LOG_DEFAULT_BACKENDS_INIT();

  NRF_LOG_FLUSH();
  LOG_system_init("------------LOG INIT-------------");
  
  twi_init();
  led_init();
  
  nrf_gpio_cfg_input(45,NRF_GPIO_PIN_PULLUP);

  scan_address();
  
  //---------------LOGIC
  pct_software_reset();
  pct_boot_ready_check();
  pct_suspend();
  pct_resume();
  pct_boot_ready_check();
  
  dfu_get_bank();

  pct_get_version_and_projectid();

  dfu_set_usr_bank();

  #if 0
  while(true)
  {
    pct_read_status();
    pct_report_access();
    pct_ack_and_clear_int();
  }
  #endif


#if 1

  dfu_enter_engineering_mode();
  
  dfu_power_up_flash();
  

  if(1)
  {//flash update process
    LOG_temp("FLASH UPDATE PROCESS"); 
    LOG_temp("data_size: %d",pixart_dfu_hex_size);
    uint16_t data_size = pixart_dfu_hex_size;
    const uint8_t* data = pixart_dfu_hex;
    uint16_t sector = 0x00; // 0x0E

    uint16_t sector_count = 0;
    uint16_t page_count = 0;
    uint16_t N = (data_size>>12) + (((data_size & 0x00000fff) == 0) ? 0 : 1);

    for(sector_count = 0; sector_count < N; sector_count++)
    {
      dfu_erase_flash_sector(sector + sector_count);
      for(page_count = 0; page_count<16; page_count++)
      {
        dfu_write_256B_data_to_sram(data, data_size, (sector_count<<12)+(page_count<<8));        
        dfu_program_256B_sram_data_to_flash(sector+sector_count, page_count);
      }
    }
    LOG_temp("FLASH UPDATE PROCESS DONE\n"); 
  }

  nrf_delay_ms(100);

  dfu_exit_enginerring_mode();

  pct_boot_ready_check();
  
    
  pct_software_reset();
  pct_boot_ready_check();
  pct_suspend();
  pct_resume();
  pct_boot_ready_check();
  
  pct_get_version_and_projectid();


#endif


while(true)
{

}
}

/** @} */
